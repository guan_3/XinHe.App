import request from '@/utils/request'


// 登录方法
export function getXhCustomerOrderList(query) { 
  return request({
    'url': '/XinHe/XhCustomerOrder/list',
    headers: {
      isToken: true
    },
    'method': 'get',
    'params': query
  })
}

// 登录方法
export function getXhCustomerOrder(billNo) { 
  return request({
    'url': '/XinHe/XhCustomerOrder/detail_by_billNo',
    headers: {
      isToken: true
    },
    'method': 'get',
    'params': {billNo}
  })
}
 
// 登录方法
export function getDetpTreeSelect() { 
  return request({
    'url': '/system/dept/treeselect',
    headers: {
      isToken: true
    },
    'method': 'get',
  })
}