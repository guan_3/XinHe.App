import request from '@/utils/request'


// 登录方法
export function getPaichanbaoOrderList(query) { 
  return request({
    'url': '/XinHe/XhPaichanbaoOrder/listapp',
    headers: {
      isToken: true
    },
    'method': 'get',
    'params': query
  })
}

// 登录方法
export function getPaichanbaoOrder(id) { 
  return request({
    'url': '/XinHe/XhPaichanbaoOrder/detail_by_id',
    headers: {
      isToken: true
    },
    'method': 'get',
    'params': {id}
  })
}
 
// 登录方法
export function getDetpTreeSelect() { 
  return request({
    'url': '/system/dept/treeselect',
    headers: {
      isToken: true
    },
    'method': 'get',
	'params': {isIgnoreFilter:true}
  })
}

// 登录方法
export function transfer(id,targetDeptId) { 
  return request({
    'url': '/XinHe/XhPaichanbaoOrder/transfer',
    headers: {
      isToken: true
    },
    'method': 'post', 
	data:{id,targetDeptId}
  })
}

// 登录方法
export function complete(id) { 
  return request({
    'url': '/XinHe/XhPaichanbaoOrder/complete',
    headers: {
      isToken: true
    },
    'method': 'post', 
	data:{id}
  })
}

